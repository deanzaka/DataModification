
public class AddrLineFour {

	public AddrLineFour() {
		
	}
	
	public String joinToThree(String input) {
		StringUtil join = new StringUtil();
		
		String[] temp =  input.split("\\x7C");
		String lineThree = temp[25] + " " + temp[51];
		temp[25] = lineThree;
		temp[51] = null;
		
		return join.unsplit(temp, 0, 92, "|");
	}
}
