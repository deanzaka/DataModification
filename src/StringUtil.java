
public class StringUtil {
	public StringUtil() {
		
	}
	
	public String unsplit(String[] parts, int index, int length, String splitter)
	{
		if (parts == null) return null;
		if ((index < 0) || (index >= parts.length)) return null;
		if (index+length > parts.length) return null;
		
		StringBuilder buf = new StringBuilder();
		for (int i = index; i < index+length; i++)
		{
		  if (parts[i] != null) buf.append(parts[i]);
		  buf.append(splitter);
		}
		
		// remove the trailing splitter
		buf.setLength(buf.length()-splitter.length());
		return buf.toString();
	}
}
