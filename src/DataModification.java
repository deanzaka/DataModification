import java.io.*;
import java.util.Scanner;

public class DataModification {
	
	private static Scanner in;

	public static void main(String[] args) {
		AddrLineFour adlf = new AddrLineFour();
		
		String fileInput, fileOutput;
		in = new Scanner(System.in);
		
		if (args.length == 2) {
			fileInput = args[0];
			fileOutput = args[1];
		} else {
			System.out.println("Set input file: ");
			fileInput = in.nextLine();
			System.out.println("Set output file: ");
			fileOutput = in.nextLine();
		}
		
		//fileInput = "/home/deanzaka/Downloads/TARGET/TEST/input.sif";
		//fileOutput = "/home/deanzaka/Downloads/TARGET/TEST/output.sif";
		
		String line = null;
		
		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = null;
		
		try {
			FileReader fileReader = new FileReader(fileInput);
			bufferedReader = new BufferedReader(fileReader);
			FileWriter fileWriter = new FileWriter(fileOutput);
			bufferedWriter = new BufferedWriter(fileWriter);
			
			long tStart = System.currentTimeMillis();
			int count = 1;
			while((line = bufferedReader.readLine()) != null) {
				long tEnd = System.currentTimeMillis();
				long tDelta = tEnd - tStart;
//				double elapsedSeconds = tDelta / 1000.0;
				System.out.println("Line number: " + count + "| Time elapsed: " + tDelta + " ms");;
				bufferedWriter.write(adlf.joinToThree(line));
				bufferedWriter.newLine();
				count++;
			}
		} catch (FileNotFoundException ex) {
			System.out.println("File " + fileInput + " not found.");
		} catch (IOException ex) {
			System.out.println("Error reading file");
		} finally {
			try {
				if(bufferedReader != null) {
					bufferedReader.close();
				}
				if(bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (IOException e) {
				
			}
		} 
	}

}
